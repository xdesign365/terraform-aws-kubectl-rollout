terraform {
  required_version = "~> 1.0"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
    }
  }
}

locals {
  function_name = "${var.lambda_function_name}-${var.deployment_name}"
}

data "aws_caller_identity" "main" {}

resource "aws_sns_topic_subscription" "main" {
  topic_arn = var.topic_arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.eks_deploy_lambda.arn
}

resource "aws_cloudwatch_log_group" "eks_deploy_lambda_log_group" {
  name              = "/aws/lambda/${local.function_name}"
  retention_in_days = 14
}

resource "aws_lambda_function" "eks_deploy_lambda" {
  filename         = "${var.lambda_function_name}.zip"
  function_name    = local.function_name
  role             = aws_iam_role.eks_deploy_lambda_role.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.eks_deploy_lambda.output_base64sha256
  runtime          = "nodejs14.x"
  timeout          = 60
  memory_size      = 256

  environment {
    variables = {
      CLUSTER_NAME  = var.cluster_name
      IAM_AUTH_PATH = var.aws_auth_command
      REGION        = var.region
      DEPLOYMENT    = var.deployment_name
      ROLE_ARN      = aws_iam_role.eks_deploy_lambda_role.arn
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks_deploy_lambda_attachment,
    aws_cloudwatch_log_group.eks_deploy_lambda_log_group,
  ]
}

data "archive_file" "eks_deploy_lambda" {
  type        = "zip"
  source_dir  = "${path.module}/deploy-lambda"
  output_path = "${var.lambda_function_name}.zip"
}

output "lambda_role_arn" {
  value = aws_iam_role.eks_deploy_lambda_role.arn
}