const k8s = require('@kubernetes/client-node');
const { EKSClient, DescribeClusterCommand } = require("@aws-sdk/client-eks");
const getConfigForOptions = require('./utils/getConfigForOptions');

// Set Env
const CLUSTER_NAME = process.env.CLUSTER_NAME;
const REGION = process.env.REGION;
const IAM_AUTH_PATH = process.env.IAM_AUTH_PATH || "./bin/aws-iam-authenticator"; 
const deployment = process.env.DEPLOYMENT
const namespace = process.env.NAMESPACE || 'default';
const cUser = process.env.ROLE_ARN;

// Create EKS Client
const clientEKS = new EKSClient({ region: REGION });
const kc = new k8s.KubeConfig()

exports.handler = async function (event) {
    console.log(`Running as ${cUser}`);
    try {
        const describeParams = {
            name: CLUSTER_NAME
        };
        const clusterInfo = await clientEKS.send(new DescribeClusterCommand(describeParams));
        const { arn, certificateAuthority, endpoint } = clusterInfo.cluster;
        const optionsConfig = getConfigForOptions(CLUSTER_NAME, REGION, arn, arn, cUser, certificateAuthority.data, endpoint, "Config", IAM_AUTH_PATH)

        kc.loadFromOptions(optionsConfig);  

        const k8sApi = kc.makeApiClient(k8s.AppsV1Api);

        const dep = await k8sApi.readNamespacedDeploymentScale(deployment, namespace);
        const item, { metadata } = dep.body;

        delete metadata.uid
        delete metadata.selfLink
        delete metadata.resourceVersion
        delete metadata.creationTimestamp
        
        console.log('Setting to 0; waiting for a second')
        await k8sApi.replaceNamespacedDeploymentScale(deployment, namespace, {
            ...item,
            spec: { replicas: 0 }
        })

        setTimeout(async () => {
            console.log('Setting to 1; complete')
            await k8sApi.replaceNamespacedDeploymentScale(deployment, namespace, {
                ...item,
                spec: { replicas: 1 }
            })
        }, 3000)


        return {
            namespace,
            deployment,
            status: 'restarted'
        }

    } catch (err) {
        console.error(err)
        throw new Error(`Error During listNamespacedPod call: ${err}`);
    }
}