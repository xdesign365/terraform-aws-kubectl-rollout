resource "aws_iam_policy" "eks_deploy_lambda_policy" {
  name        = "${local.function_name}-policy"
  path        = "/"
  description = "IAM policy EKS Deploy Lambda"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        "Resource" : "${aws_cloudwatch_log_group.eks_deploy_lambda_log_group.arn}:*",
        "Effect" : "Allow"
        }, {
        "Action" : [
          "ec2:DescribeInstances",
          "ec2:CreateNetworkInterface",
          "ec2:AttachNetworkInterface",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DeleteNetworkInterface"
        ],
        "Resource" : "*",
        "Effect" : "Allow"
      },
      {
        "Action" : [
          "eks:DescribeCluster"
        ],
        "Resource" : "arn:aws:eks:${var.region}:${data.aws_caller_identity.main.account_id}:cluster/${var.cluster_name}",
        "Effect" : "Allow"
      }
    ]
  })

  depends_on = [
    aws_cloudwatch_log_group.eks_deploy_lambda_log_group
  ]
}

resource "aws_iam_role" "eks_deploy_lambda_role" {
  name = "${local.function_name}-role"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "lambda.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "eks_deploy_lambda_attachment" {
  role       = aws_iam_role.eks_deploy_lambda_role.name
  policy_arn = aws_iam_policy.eks_deploy_lambda_policy.arn
}