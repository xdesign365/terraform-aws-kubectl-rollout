
variable "region" {
  default = "eu-west-1"
}

# variable "vpc_id"{}

# variable "subnets_ids"{
#   type = set(string)
# }

variable "sg_ingress_cidr" {
  default = "0.0.0.0/0"
}

variable "lambda_function_name" {
  default = "eks-deploy"
}

variable "cluster_name" {
  default = "xdesign-primary"
}

variable "aws_auth_command" {
  default = "./bin/aws-iam-authenticator"
}

variable "deployment_name" {}
variable "topic_arn" {}